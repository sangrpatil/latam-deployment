export const environment = {
  production: true,
  baseUrl: 'http://amraelp00007293.pfizer.com:30080',
  socketURL: 'http://amraelp00007293.pfizer.com:30080',
  engineURL: 'http://amraelp00007293.pfizer.com:30080/server/api/',
  apiSuffix: '/admin/api'
};