export const environment = {
  production: true,
  baseUrl: 'http://amraelp00007159.pfizer.com:30080',
  socketURL: 'http://amraelp00007159.pfizer.com:30080',
  engineURL: 'http://amraelp00007159.pfizer.com:30080/server/api/',
  apiSuffix: '/admin/api'
};
