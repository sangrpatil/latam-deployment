export const environment = {
  production: true,
  baseUrl: 'http://minions.pfizer.com:30080',
  socketURL: 'http://minions.pfizer.com:30080',
  engineURL: 'http://minions.pfizer.com:30080/server/api/',
  apiSuffix: '/admin/api'
};